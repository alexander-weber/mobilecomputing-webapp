/**
 * 
 */
( function() {

	document.addEventListener("DOMContentLoaded", function(event) {
		var threshold = 50;
		var lightLevel = -1;
		var deviceLightAvailable = ("ondevicelight" in window);

		var page = document.getElementById('frontpage');
		var darkModeSwitch = document.getElementById('dark-mode-switch');
		var lightLevelView = document.getElementById('light-level');
		updateLightLevelView(lightLevelView, lightLevel);

		var thresholdView = document.getElementById('toggle-threshold');
		updateThresholdView(thresholdView, threshold);

		var buttonDecreseThreshold = document.getElementById('decrease-threshold');
		var buttonIncreseThreshold = document.getElementById('increase-threshold');

		document.getElementById('automatic-switch').classList.add("display-none");
		// Listen to clicks on the manual Dark Mode switch.
		darkModeSwitch.addEventListener("click", function(){
			if (darkModeSwitch.classList.contains("activated")) {
				darkModeSwitch.classList.remove("activated");
				page.classList.remove("dark-mode");
			} else {
				darkModeSwitch.classList.add("activated");
				page.classList.add("dark-mode");
			}
		});
		if (deviceLightAvailable) {
			// Listen to ambient light changes if the sensor is supported.
			window.addEventListener('devicelight', function(event) {
				// Wait until the first value is measured, then switch to automatic mode.
				if (lightLevel == -1) {
					document.getElementById('manual-switch').classList.add("display-none");
					document.getElementById('automatic-switch').classList.remove("display-none");
					page.classList.add("device-light-available")
				}
				lightLevel = event.value;
				// Update the real time device light view.
				updateLightLevelView(lightLevelView, lightLevel);
				// Enable Dark Mode when the light level is below the threshold.
				if (lightLevel > threshold) {
					page.classList.remove("dark-mode");
				} else {
					page.classList.add("dark-mode");
				}
			});
			// Listen to clicks on the decrease/increase threshold button and adjust the number and view accordingly.
			buttonDecreseThreshold.addEventListener("click", function(){
				if (threshold > 0) {
					threshold = threshold - 5;
					updateThresholdView(thresholdView, threshold);
				}
			});
			buttonIncreseThreshold.addEventListener("click", function(){
				if (threshold < 200) {
					threshold = threshold + 5;
					updateThresholdView(thresholdView, threshold);
				}
			});
		}
	});

	function updateThresholdView(thresholdView, threshold) {
		thresholdView.innerHTML = "" + threshold;
	}
	function updateLightLevelView(lightLevelView, value) {
		lightLevelView.innerHTML = "" + value;
	}
} )();