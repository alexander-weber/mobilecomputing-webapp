# MobileComputing WebApp

## Usage
To switch to Dark Mode automatically based the light in your environment, make sure you have access to the Ambient Light sensor: https://whatwebcando.today/ambient-light.html
It is supported on Firefox and can be enabled under about:config by setting device.sensors.ambientLight.enabled=true.